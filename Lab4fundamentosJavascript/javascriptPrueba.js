
var numero = prompt("Ingresa un numero");
var arregloVacio;
var matrizVacia;


function generar_tabla(){
	//Obtener la referencia del elemento body
	var body = document.getElementsByTagName("section")[0];
	//Crea un elemento <table> y un elemento <tbody>
	var tabla = document.createElement("table");
	var tblBody = document.createElement("tbody");
	
	//Crea las celdas
	for(var i=1; i<=numero;i++){
		//Crea las hileras de la tabla
		var hilera = document.createElement("tr");
		for(var j=0;j <= 2;j++){

			var celda = document.createElement("td");
			var cuadrado = Math.pow(i,2);
			var cubo = Math.pow(i,3);
			if(j==0){
				var textoCelda=document.createTextNode(i);
			}
			else if(j == 1){
				var textoCelda=document.createTextNode(cuadrado);
			}
			else if(j == 2){
				var textoCelda=document.createTextNode(cubo);
			}
			//var textoCelda = document.createTextNode("celda en la hilera "+i+", columna"+j);
			celda.appendChild(textoCelda);
			hilera.appendChild(celda);
		}
		tblBody.appendChild(hilera);
	}

	tabla.appendChild(tblBody);
	body.appendChild(tabla);
	tabla.setAttribute("border","5");
}

function resultadoSuma(){
	var a=-1000;
	var b=1000;
	var val1 = Math.round(Math.random()*(b-a)+parseInt(a));
	var val2 = Math.round(Math.random()*(b-a)+parseInt(a));
	var suma = val1 + val2; 
	var valorDelUsuario = prompt("Cual es el valor de la suma entre "+val1+" y "+val2+"?");
	if(suma == valorDelUsuario){
		alert("El resultado de la suma es correta!!!");
	}
	else{
		alert("El resultado de la suma es incorrecta!!!, prueba de nuevo");
	}
}

function ped(){
	arregloVacio = new Array();
	var divi = document.getElementById("textDIV");
	divi.innerHTML = "El numero total de elementos del arreglo es: "+numero;
	llenarArregloAleatorio(arregloVacio);
	contador(arregloVacio);	
}

function llenarArregloAleatorio(arregloVacio){
	var a=-10000;
	var b=10000;
	for(var k=0;k<numero;k++){
		arregloVacio[k] = Math.round(Math.random()*(b-a)+parseInt(a));
	}
	for(var k=0;k<numero;k++){
		var division = document.getElementById("textDIV2");
		division.innerHTML = division.innerHTML+"val "+k+" = "+arregloVacio[k]+"<br>";
	}
}

function contador(arregloVacio){
	//llenarArregloAleatorio(arregloVacio);
	var contCeros=0;
	var contMayorDeCero = 0;
	var contMenorDeCero = 0;
	for(var k=0;k < numero;k++){
		if(arregloVacio[k] == 0){
			contCeros++;
		}
		else if(arregloVacio[k] > 0){
			contMayorDeCero++;
		}else{
			contMenorDeCero++;
		}
	}
	var divis = document.getElementById("textDIV3");
	divis.innerHTML = "El numero total de ceros en el arreglo es: "+contCeros+", la cantidad de numeros menores que cero son: "+contMenorDeCero+" y la cantidad de numeros mayores que cero son: "+contMayorDeCero;
}


window.onload = lanzadera;


function lanzadera(){
	generar_tabla();
	resultadoSuma();
	//llenarArregloAleatorio2();
	ped();
	promedios();
}


function llenarArregloAleatorio2(){
	//Obtener la referencia del elemento body
	var body = document.getElementsByTagName("section")[1];
	//Crea un elemento <table> y un elemento <tbody>
	var tabla = document.createElement("table");
	var tblBody = document.createElement("tbody");
	var a=-10000;
	var b=10000;
	var contCeros=0;
	var contMayorDeCero = 0;
	var contMenorDeCero = 0;
	//Crea las celdas
	for(var i=1; i<=1;i++){
		//Crea las hileras de la tabla
		var hilera = document.createElement("tr");
		for(var j=0;j < numero;j++){

			var celda = document.createElement("td");
			var numeroAleatorio = Math.round(Math.random()*(b-a)+parseInt(a));
			var textoCelda=document.createTextNode(numeroAleatorio);
			if(numeroAleatorio == 0){
				contCeros++;
			}
			else if(numeroAleatorio > 0){
				contMayorDeCero++;
			}
			else if(numeroAleatorio < 0){
				contMenorDeCero++;
			}
			//var textoCelda = document.createTextNode("celda en la hilera "+i+", columna"+j);
			celda.appendChild(textoCelda);
			hilera.appendChild(celda);
		}
		tblBody.appendChild(hilera);
	}

	tabla.appendChild(tblBody);
	body.appendChild(tabla);
	tabla.setAttribute("border","5");
	var divi = document.getElementById("textDIV4");
	divi.innerHTML = "El numero total de ceros en el arreglo es: "+contCeros+" , la cantidad de numeros menores que cero son: "+contMenorDeCero+" y la cantidad de numeros mayores que cero son: "+contMayorDeCero;
}

function promedios(){
	matrizVacia = new Array();
	var contenedor = document.getElementById("textDIV5");
	contenedor.innerHTML = "El numero total de elementos de la matriz es "+(numero*numero);
	llenarMatriz(matrizVacia);
	promedioArreglos(matrizVacia);
}

function llenarMatriz(matrizVacia){
	var a=1000;
	var b=100;
	for(var k=0;k<numero;k++){
		matrizVacia[k] = new Array(numero);
		for(var m=0;m<numero;m++){
			matrizVacia[k][m] = Math.round(Math.random()*(b-a)+parseInt(a));
		}
	}
	for(var k=0;k<numero;k++){
		for(var m=0;m<numero;m++){
			var contenedor6 = document.getElementById("textDIV6");
			contenedor6.innerHTML = contenedor6.innerHTML+"val ["+k+"]["+m+"]="+matrizVacia[k][m]+"  ";
		}
		contenedor6.innerHTML = contenedor6.innerHTML+"<br>"
	}
}

function promedioArreglos(matrizVacia){
	//llenarArregloAleatorio(arregloVacio);
	var arregloAux = Array(numero);
	var contenedor7 = document.getElementById("textDIV7");
	for(var k=0;k < numero;k++){
		var promedios=0;
		var sum=0;
		for(var m=0;m<numero;m++){
			sum=sum+matrizVacia[k][m];
		}
		promedios=sum/numero;
		arregloAux[k]=promedios;
		
		contenedor7.innerHTML = contenedor7.innerHTML + "El promedio en la fila "+k+" es "+arregloAux[k]+"<br>";
	}	
}


