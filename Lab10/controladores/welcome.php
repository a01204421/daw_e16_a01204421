<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="estilos10/estilos10.css">
		<title>Lab9: Manejo de formas PHP</title>
	</head>
	<body>
		<header>
			<div class="container text-center">
				<h2>Lab 9: &nbsp;<small class="subtext">Manejo de formas con php y modelo de capas</small></h2>	
			</div>		
		</header>
		<section>
			<div class="container">
				<?php 
					$nombre = $_POST['nombre'];
					$mail = $_POST['mail'];
					$psw = $_POST['psw'];

					echo "Bienvenido ".$nombre." a esta nueva pagina"."<br>";
					echo "Tu correo ingresado es: ".$mail;
				?>
			</div>
		</section>
		<section>
			<div class="container">
				<h2>Preguntas a responder</h2><br>
				<div class="pregunta">
					<strong>¿Por qué es una buena práctica separar el controlador de la vista?.</strong>
				</div>
				<div class="respuesta">
					Debido a que es un patrón de diseño de software que convierte una aplicación en un paquete modular fácil de mantener y mejora la rapidez del desarrollo. La separación de las tareas de tu 
					aplicación en modelos, vistas y controladores hace que su aplicación sea además muy ligeras de entender. Las nuevas características se añaden fácilmente y agregar cosas nuevas al código 
					viejo se hace muy sencillo. El diseño modular también permite a los desarrolladores y los diseñadores trabajar simultáneamente, incluyendo la capacidad de hacer prototipos rápidos.
					La separación también permite a los desarrolladores hacer cambios en una parte del la aplicación sin afectar a los demás.
				</div>
				<div class="pregunta">
					<strong>Aparte de los arreglos $_POST y $_GET, ¿qué otros arreglos están predefinidos en php y cuál es su función?</strong>
				</div>
				<div class="respuesta">
					<dl class="dl-horizontal">
						<dt>$GLOBALS</dt>
						<dd> Hace referencia a todas las variables disponibles en el ámbito global</dd>
						<dt>$_SERVER</dt>
						<dd> Información del entorno del servidor y de ejecución</dd>
						<dt>$_GET</dt>
						<dd>Variables HTTP GET. Un array asociativo de variables pasado al script actual vía parámetros URL.</dd>
						<dt>$_POST</dt>
						<dd>Variables POST de HTTP. Un array asociativo de variables pasadas al script actual a través del método POST
						 de HTTP cuando se emplea application/x-www-form-urlencoded o multipart/form-data como Content-Type de HTTP en la petición.
						</dd>
						<dt>$_FILES</dt>
						<dd>Variables de Carga de Archivos HTTP. Una variable tipo array asociativo de elementos cargados al script actual a través del método POST.</dd>
						<dt>$_COOKIE</dt>
						<dd>Cookies HTTP. Una variable tipo array asociativo de variables pasadas al script actual a través de Cookies HTTP.</dd>
						<dt>$_SESSION</dt>
						<dd> Variables de sesión. Es un array asociativo que contiene variables de sesión disponibles para el script actual. 
							Ver la documentación de Funciones de sesión para más información sobre su uso.</dd>
						<dt>$_REQUEST</dt>
						<dd>Variables HTTP Request. Un array asociativo que por defecto contiene el contenido de $_GET, $_POST y $_COOKIE.</dd>
						<dt>$_ENV</dt>
						<dd>Variables de entorno. Una variable tipo array asociativo de variables pasadas al script actual a través del método del entorno.</dd>
					</dl>		
				</div>
				<div class="pregunta">
					<strong>Explora las funciones de php, y describe 2 que no hayas visto en otro lenguaje y que llamen tu atención.</strong>
				</div>
				<div class="respuesta">
					get_loaded_extensions — Devuelve un array con los nombres de todos los módulos compilados y cargados.<br>
					Esta función devuelve los nombres de todos los módulos compilados y cargados en el intérprete de PHP.
				</div>
			</div>
		</section>
		<footer >
			<div class="container text-center">
				<h3>Derechos reservados &reg; 2016 </h3>	
			</div>		
		</footer>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	</body>
</html>