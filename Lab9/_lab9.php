  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

      <!-- Optional theme -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    </head>
     <body>
          <header>
              <div class="container">
                  <h1> Mi laboratorio 9&nbsp;<small>Introduccion a php</small></h1>
              </div>
          </header>
          <?php include_once("prueba.php"); ?>
          <section>
              <article>
                  <div class="container-fluid">
                      <p class="bg-primary">
                        ¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.           
                      </p>
                      <p class="bg-info">
                        La funcion phpinfo(), nos muestra información, sobre el sistema operativo que se
                      esta utilizando, el servidor apache y version con la cual se esta trabajando y toda la información sobre PHP
                      </p>
                 </div>
                  <div class="container-fluid">
                      <div class="bg-primary">
                          ¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?
                      </div>
                      <div class="bg-info">
                          El primer paso consiste en comprobar que tanto PHP como algunas de sus extensiones están correctamente instaladas y configuradas.
                      </div>
                  </div>
                  <div class="container-fluid">
                      <div class="bg-primary">
                            ¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica. 
                      </div>
                      <div class="bg-info">
                            Es porque los lenguajes del lado del servidor, son scripts que interactuan con la base de datos o el servidor remoto, ya que son diseñados para que funcionen de esa forma. Cuando son diseñados del lado del servidor se reduce la cantidad de errores o problemas de compatibilidad.
                      </div>
                  </div>   
              </article>
          </section>
          <footer>
                <h2>Derechos reservados 2016</h2>
          </footer>
          <!-- Latest compiled and minified JavaScript -->
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
     </body>
</html>

