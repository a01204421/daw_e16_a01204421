
<?php
/*
    $saludo  = "hola";
    echo $saludo;
    echo "<br >";
    echo "<table ><tbody>";
    for($i=0;$i < 10;$i++){
        echo "<tr>";
        echo "<td>".$i;
        echo "</td>";
        echo "<td>";
        echo $i*$i;
        echo "</td>";
        echo "<td>";
        echo $i*$i*$i;
        echo "</td>";
        echo "</tr>";
        
    }*/
?>


<div class="container">
    <h3 class="bg-success">
        Ejercicio 1. Una función que reciba un arreglo de números y devuelva su promedio
    </h3>
    <p class="bg-info">
        A continuacion se enlista los elementos del array
    </p>
    <?php 
        $numero = 50;
        $mi_array = array();
        $contador =0;
        $promedio=0;
        for($i=0;$i<$numero;$i++){
            $mi_array[]= rand(1,100);
        }
        for($i=0;$i<$numero;$i++){
            $contador = $contador + $mi_array[$i];
        }
        $promedio = $contador/$numero;
        echo "<pre>";
        print_r($mi_array);
        //var_dump($mi_array);
        echo "<p>El promedio en el arreglo es: $contador/$numero = $promedio</p><br><br>";
        echo "</pre>";
        
    ?>
</div>

<div class="container">
    <h3 class="bg-success">
        Ejercicio 2. Una función que reciba un arreglo de números y devuelva su mediana
    </h3>
    <p class="bg-info">
        A continuacion se enlista los elementos del array.
    </p>
    <?php 
        $numero = 50;
        $mi_array = array();
        $media=0;
        $mediana = 0;
        for($i=0;$i<$numero;$i++){
            $mi_array[]= rand(1,100);
        }
        
        echo "<pre>";
        echo "<br>Esto es el arreglo sin ordenar<br>";
        print_r($mi_array);
        //var_dump($mi_array);
        echo "</pre>";
        sort($mi_array,SORT_REGULAR);
        echo "<pre>";
        echo "<br>Esto es el arreglo ordenado<br>";
        print_r($mi_array);
        //var_dump($mi_array);
        
        for($i=0;$i<$numero;$i++){
            $mediana = $mi_array[$numero/2];
        }
        echo "<br>La mediana es: $mediana<br><br>";
        echo "</pre>";
    ?>
</div>

<div class="container">
    <h3 class="bg-success">
        Ejercicio 3. Una función que reciba un arreglo de números y muestre la lista de números, y como ítems de una lista html muestre el promedio, la media, y el arreglo ordenado de menor a mayor, y posteriormente de mayor a menor
    </h3>
    <p class="bg-info">
        A continuacion se muestran los elementos del arreglo.
    </p>
    <?php
        $numero = 50;
        $mi_array = array();
        $contador=0;
        $media=0;
        $mediana = 0;
        for($i=0;$i<$numero;$i++){
            $mi_array[]= rand(1,100);
        }
        
        echo "<pre>";
        echo "<br>Esto es el arreglo sin ordenar<br>";
        print_r($mi_array);
        //var_dump($mi_array);
        for($i=0;$i<$numero;$i++){
            $contador = $contador + $mi_array[$i];
        }
        $media = $contador/$numero;
        echo "<br> El promedio del arreglo es: $contador/$numero= $media<br>";
        echo "</pre>";
        
        sort($mi_array,SORT_REGULAR);
        echo "<pre>";
        echo "<br>Esto es el arreglo ordenado de menor a mayor<br>";
        print_r($mi_array);
        $mediana = $mi_array[$numero/2];
        echo "<br>la mediana es: $mediana<br>";
        echo "</pre>";
        arsort($mi_array);
        
        echo "<pre>";
        echo "<br>Esto es el arreglo ordenado de mayor a menor<br>";
        print_r($mi_array);
        echo "</pre>";
    ?>
</div>

<div class="container">
    <h3 class="bg-success">
        Ejercicio 4. Una función que imprima una tabla html, que muestre los cuadrados y cubos de 1 hasta un número n
    </h3>
    <p class="bg-info">
        A continuacion se muestra la tabla.
    </p>
    <table class="table table-striped">
        <tbody>
            <tr>
                <td><?= "valor n";?></td>
                <td><?= "valor n*n";?></td>
                <td> <?= "valor n*n*n";?></td>
            </tr>
            <?php for ($i = 0; $i < 10; $i++): ?>
                <tr>
                    <td><?= $i;?></td>
                    <td><?= $i*$i;?></td>
                    <td> <?= $i*$i*$i;?></td>
                </tr>
            <?php endfor ; ?>
         </tbody>
    </table>   
</div>
<!--
<ol class="striped">
    <?php for ($i = 0; $i < 10; $i++): ?>
       <li> <?= $i?></li>
    <?php endfor ; ?>
</ol> -->
