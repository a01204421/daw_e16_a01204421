/*Ejercicio 1*/
SELECT sum(cantidad) as 'Cantidad total', sum(costo + (costo * (porcentajeImpuesto/100))) as 'Importe total'
FROM materiales
WHERE Fecha >= 01-01-1997 AND fecha<=31-12-1997
GROUP BY 'Cantidad total'

/*Ejercicio 2*/
SELECT RazonSocial, count(cantidad) as 'Cantidad total', SUM(costo + (costo * (porcentajeImpuesto/100))) as 'Importe total'
FROM Entregan E, proveedores P, materiales M
WHERE E.RFC = P.RFC AND M.clave = E.clave
GROUP BY RazonSocial


/*Ejercicio 3*/
SELECT E.clave,descripcion, sum(cantidad) as 'Cantidad total', MAX(cantidad) as 'Maxima cantidad', MIN(cantidad) as 'Minima cantidad', sum(costo * (1 + (porcentajeImpuesto/100) )) as 'Importe total'
FROM materiales M,entregan E
WHERE M.clave = E.clave
GROUP BY E.clave,descripcion
HAVING sum(costo * (1 + (porcentajeImpuesto/100) )) > 400

/*Ejercicio 4*/
SELECT RazonSocial, E.clave, descripcion, AVG(cantidad) as 'Cantidad promedio'
FROM Proveedores P,materiales M, Entregan E
WHERE P.RFC = E.RFC AND E.clave = M.clave
GROUP BY RazonSocial,E.clave,descripcion
HAVING AVG(cantidad) < 500

/*Ejercicio 5*/
SELECT RazonSocial, E.clave, descripcion, AVG(cantidad) as 'Cantidad promedio'
FROM Proveedores P,materiales M, Entregan E
WHERE P.RFC = E.RFC AND E.clave = M.clave
GROUP BY RazonSocial,E.clave,descripcion
HAVING AVG(cantidad) < 370
UNION
SELECT RazonSocial, E.clave, descripcion, AVG(cantidad) as 'Cantidad promedio'
FROM Proveedores P,materiales M, Entregan E
WHERE P.RFC = E.RFC AND E.clave = M.clave
GROUP BY RazonSocial,E.clave,descripcion
HAVING AVG(cantidad) > 450

/*Insertar datos*/
INSERT INTO materiales VALUES (2000,'Puerta Gris',500,3.01)
INSERT INTO materiales VALUES (2010,'Puerta Roja',510,3.01)
INSERT INTO materiales VALUES (2020,'Puerta Verde',520,3.01)
INSERT INTO materiales VALUES (2030,'Puerta Negro',530,3.01)
INSERT INTO materiales VALUES (2040,'Puerta Azul',540,3.01)


/*Ejercicio 6*/
SELECT Descripcion
FROM Materiales
WHERE NOT EXISTS (SELECT * FROM Entregan WHERE Materiales.Clave = Entregan.CLave)

/*Ejercicio 7*/
SELECT RazonSocial
FROM proveedores P, entregan E, proyectos PR
WHERE P.RFC = E.RFC AND PR.Numero = E.Numero AND PR.denominacion = 'Vamos Mexico'
INTERSECT
SELECT RazonSocial
FROM proveedores P, entregan E, proyectos PR
WHERE P.RFC = E.RFC AND PR.Numero = E.Numero AND PR.denominacion = 'Queretaro Limpio'

/*Ejercicio 8*/

SELECT Descripcion
FROM Materiales
WHERE NOT EXISTS(SELECT * 
				FROM Proyectos, Entregan 
				WHERE Materiales.Clave = Entregan.Clave
                	AND Proyectos.Numero = Entregan.Numero
                    AND Proyectos.Denominacion = 'CIT Yucatan')

/*Ejercicio 9*/
SELECT RazonSocial, AVG(cantidad) as 'Cantidad promedio'
FROM proveedores PR, Entregan E
WHERE PR.RFC = E.RFC 
GROUP BY RazonSocial
HAVING AVG(cantidad) > (SELECT AVG(cantidad) FROM Entregan E, Proveedores PR WHERE PR.RFC = 'VAGO780901')

/*Ejercicio 10*/
SELECT RFC, RazonSocial,sum(cantidad) as 'Cantidad total'
FROM proveedores PR, proyectos P,entregan E
WHERE PR.RFC = E.RFC AND P.Numero = E.Numero AND P.denominacion = 'Infonavit Durango' AND fecha >= 01-01-2000 AND fecha <= 31-12-2000
GROUP BY RFC,RazonSocial
HAVING sum(cantidad) > (SELECT RFC, RazonSocial,sum(cantidad) as 'Cantidad total'
						FROM proveedores PR, proyectos P,entregan E
						WHERE PR.RFC = E.RFC AND P.Numero = E.Numero AND P.denominacion = 'Infonavit Durango' AND fecha >= 01-01-2001 AND fecha <= 31-12-2001
						GROUP BY RFC,RazonSocial)