/*Esto es una consulta a una tabla completa*/
SELECT * FROM materiales

/*Esto es una consulta con una condicion de seleccion*/
SELECT * FROM materiales 
WHERE clave=1000 

/*Esto es una consulta con proyeccion*/
SELECT clave,rfc,fecha FROM entregan 

/*Esto es una consulta con un join natural*/
SELECT * FROM materiales,entregan 
WHERE materiales.clave = entregan.clave 

/*Esto es una consulta con un join con criterio especifico*/
SELECT * FROM entregan,proyectos 
WHERE entregan.numero < = proyectos.numero 

/*Esto es una consulta con una union*/
(SELECT * FROM entregan WHERE clave=1450) 
UNION
(SELECT * FROM entregan WHERE clave=1300) 

/*Esto es una consulta con interseccion*/
(SELECT clave FROM entregan WHERE numero=5001) 
INTERSECT
(SELECT clave FROM entregan WHERE numero=5018) 

/*Esto es una consulta con diferencia*/
(SELECT * FROM entregan) 
EXCEPT
(SELECT * FROM entregan WHERE clave=1000) 

/*Esto es una consulta con producto cartesiano*/
SELECT * FROM entregan,materiales 


/*
Construcción de consultas a partir de una especificación 
Plantea ahora una consulta para obtener las descripciones de los materiales entregados en el año 2000. 
Recuerda que la fecha puede indicarse como '01-JAN-2000' o '01/01/00'. 
*/
SELECT descripcion FROM materiales M,entregan E
WHERE E.clave = E.clave and fecha >= '01-JAN-2000' and fecha <= '31-DEC-2000'

/*Consulta utilizando el operador distinct*/
SELECT DISTINCT descripcion FROM materiales M,entregan E
WHERE E.clave = E.clave And fecha >= '01-JAN-2000' AND fecha <= '31-DEC-2000'

/*
Obtén los números y denominaciones de los proyectos con las fechas y cantidades de sus entregas, ordenadas por número de proyecto, presentando las fechas de la más reciente a la más antigua. 
*/

SElECT P.numero,denominacion,fecha,cantidad FROM proyectos P, entregan E
WHERE P.numero = E.numero 
ORDER BY fecha desc

/*Consultas con operadores de cadena, con el %*/
SELECT * FROM materiales where descripcion LIKE 'Si%' 

/*Consultas con operadores de cadena, sin el %*/
SELECT * FROM materiales where descripcion LIKE 'Si' 

/*consultas de RFC entre A y D (iniciales)*/
SELECT RFC FROM Entregan WHERE RFC LIKE '[A-D]%'; 

/*Consultas de RFC que sean mayores alfabeticamente que A*/SELECT RFC FROM Entregan WHERE RFC LIKE '[^A]%'; 

/*Consultas de Numero en donde termine con 6 de una cantidad de 4 digitos*/
SELECT Numero FROM Entregan WHERE Numero LIKE '___6'; 


/*Consulta con intervalos*/
SELECT Clave,RFC,Numero,Fecha,Cantidad 
FROM Entregan 
WHERE Numero Between 5000 and 5010; 

SELECT Clave,RFC,Numero,Fecha,Cantidad 
FROM Entregan 
WHERE fecha Between '19980708'  and '20000806'; 


/*Consulta con operador exists*/
SELECT RFC,Cantidad, Fecha,Numero FROM [Entregan] WHERE [Numero] Between 5000 and 5010 AND Exists ( SELECT [RFC] FROM [Proveedores] WHERE RazonSocial LIKE 'La%' and [Entregan].[RFC] = [Proveedores].[RFC] )

/*consulta con los dos valores de mas arriba*/
SELECT TOP 2 * FROM Proyectos /*la consulta: SELECT TOP numero * FROM Proyectos tiene errores de sysntx*/SELECT TOP 4 * FROM Proyectos 

/*
Agrega a la tabla materiales la columna PorcentajeImpuesto con la instrucción: 
ALTER TABLE materiales ADD PorcentajeImpuesto NUMERIC(6,2); 
A fin de que los materiales tengan un impuesto, les asignaremos impuestos ficticios basados en sus claves con la instrucción: 
UPDATE materiales SET PorcentajeImpuesto = 2*clave/1000; 
esto es, a cada material se le asignará un impuesto igual al doble de su clave dividida entre diez. 
*/ALTER TABLE materiales ADD PorcentajeImpuesto NUMERIC(6,2);
UPDATE materiales SET PorcentajeImpuesto = (2*clave)/1000; 
SELECT * FROM materiales