<?php
	function conectDb(){
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "laboratorio";
		$con = mysqli_connect($servername,$username,$password,$dbname);
		//check connection
		if(!$con) {
			die("connection failed".mysqli_connect_error());
		}
		//echo "Connected successfully";
		return $con;
	}

	function closeDb($mysql){
		mysqli_close($mysql);
	}

	function getVideoGames(){
		$conn = conectDb();
		$sql = 'SELECT * FROM videojuegos';
		//$result = mysqli_query($conn,$sql);
		$result = $conn->query($sql);
		$table = '<table class="table table-hover table-bordered"><tr class="success"><th>IdVideoGame</th><th>nombre</th><th>descripcion</th><th>año</th><th>categoria</th><th>desarrolladora</th></tr>';
        $i=1;
        while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
	        $table = $table . "<tr>";
	        // use of numeric index
	        $table = $table . '</td>';
	        $table = $table . "<td>" .  $row[4];
	          // name of the column as associative index
	        $table = $table . '</td>';
	        $table = $table . "<td>" .  $row[0];
	        $table = $table . '</td>';
	        $table = $table . "<td>" .  $row[1];
	        $table = $table . '</td>';
	        $table = $table . "<td>" .  $row[2];
	        $table = $table . '</td>';
	        $table = $table . "<td>" .  $row[3];
	        $table = $table . '</td>';
	        $table = $table . "<td>" . $row[5];
	        $table = $table . '</td>';
	        $table = $table . '</tr>';
	        $i=$i+1;
        }
        $table = $table . '</table>';
           // it releases the associated results
         mysqli_free_result($result);
		closeDb($conn);
		return $table;
	}

	function guardar($name, $descripcion, $desarrolladora, $anyo, $categoria) {
	    $mysql = conectDb();
	    
	    // insert command specification 
	    $query='INSERT INTO videojuegos(name,descripcion,desarrolladora,anyo,categoria) VALUES (?,?,?,?,?) ';
	    // Preparing the statement 
	    if (!($statement = $mysql->prepare($query))) {
	        die("Preparation failed: (" . $mysql->errno . ") " . $mysql->error);
	    }
	    // Binding statement params 
	    if (!$statement->bind_param("sssss", $name, $descripcion, $desarrolladora, $anyo, $categoria)) {
	        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
	    }
	     // Executing the statement
	    if (!$statement->execute()) {
	        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
	    } 
	    mysqli_free_result($result);
	    closeDb($mysql);
	}

	function delete_by_name($id){
	    $mysql = conectDb();
	    $query='DELETE FROM `laboratorio`.`videojuegos` WHERE `videojuegos`.`IdVideojuego` = '. $id;
	    // Preparing the statement 
	    if (mysqli_query($mysql, $query)) {
	        echo "Eliminacion de registro existoso";
	    } else {
	        echo "Error deleting record: " . mysqli_error($mysql);
	    } 
	    closeDb($mysql);
	}

	function update($id, $nombre, $descripcion, $anyo, $categoria, $desarrolladora) {
	    $mysql = conectDb();
	    // insert command specification 
	    $query="UPDATE  `laboratorio`.`videojuegos` SET  
	    `nombre` =  '".$nombre."',
	    `descripcion` =  '".$descripcion."',
	    `anyo` =  '".$anyo."',
	    `categoria` =  '".$categoria."',
	    `desarrolladora` =  '".$desarrolladora."'
	    WHERE  `videojuegos`.`IdVideojuego` ='".$id."'";
	    // Preparing the statement 
	    if (!($statement = $mysql->prepare($query))) {
	        die("Preparation failed: (" . $mysql->errno . ") " . $mysql->error);
	    }
	    // Binding statement params 
	    //if (!$statement->bind_param("sssss", $nombre, $descripcion, $anyo, $categoria, $desarrolladora)) {
	    //    die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
	    //}
	     // Executing the statement
	    if (!$statement->execute()) {
	        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
	    } 
	    mysqli_free_result($result);
	    closeDb($mysql);  
	}
?>