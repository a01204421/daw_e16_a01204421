-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 17-03-2016 a las 07:07:07
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `laboratorio`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videojuegos`
--

CREATE TABLE `videojuegos` (
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `anyo` year(4) NOT NULL,
  `categoria` varchar(20) NOT NULL,
  `IdVideojuego` int(6) NOT NULL,
  `desarrolladora` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `videojuegos`
--

INSERT INTO `videojuegos` (`nombre`, `descripcion`, `anyo`, `categoria`, `IdVideojuego`, `desarrolladora`) VALUES
('Gears Of War', 'Gears of War es el primer videojuego de la Saga Gears of War, la historia se centra en el planeta Sera despues del dia E cuando los locust emergieron a la superficie y declararon la guerra a los humanos. Por lo que el objetivo principal es salvar a la humanidad de la amenaza locust, siendo Marcus Fenix el protagonista principal.', 2006, 'Shooter', 1, 'Epic Games'),
('Minecraft', 'Es un juego de construccion abierto inspirado en el cual puedes construir con los elementos que existen alrededor de todo el mapa, el principal objetivo es sobrevivir de los enemigos que habitan por todo el mundo. El personaje puede crear sus propias herramientas y construcciones.', 2011, 'Mundo Libre', 2, 'Mojang'),
('Sears Of War 2', 'Es la segunda entrega de la saga de Gears Of War, da continuacion a la batalla entre humanos y locust, sin embargo una nueva amenaza comienza a desarrollarse, los lambets... ', 2008, 'Shooter', 3, 'Epic Games'),
('Halo', 'Videojuego que se basa en la guerra de ciencia-ficcion en el cual existen seres en el universo distintos a los humanos', 2000, 'Shooter', 4, 'Microsoft Studios'),
('FIFA 16', 'Videojuego basado en el deporte de futbol, en el cual puedes simular un torneo de futbol, una copa mundial o un simple partido amistoso con un amigo.', 2016, 'Deportes', 5, 'EA Sport'),
('Far Cry 4', 'Video juego basado en las historias del viejo continente en el cual se basa en un ambiente natural bastante realista', 2015, 'Aventura', 6, 'Ubisoft');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `videojuegos`
--
ALTER TABLE `videojuegos`
  ADD PRIMARY KEY (`IdVideojuego`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
