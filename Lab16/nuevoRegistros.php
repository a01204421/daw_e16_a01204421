<?php
	require_once('util.php');
	session_start();

	include('_header.html'); 

	if (isset($_POST["name"])) {
	    guardar($_POST["name"], $_POST["descripcion"], $_POST["desarrolladora"], $_POST["anyo"], $_POST["categoria"]);
	    $_SESSION["info"] = "Registro agregado";
	    header("location: index.php");
	} else  {
	    include('add_view.html'); 
	}
	include('_preguntas.html');
	include('_footer.html'); 
	
?>