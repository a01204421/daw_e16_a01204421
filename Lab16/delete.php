<?php 
	require_once('util.php');
	session_start();
	include('_header.html'); 

	if (isset($_POST["id"])) {
	    //success
	    delete_by_name($_POST["id"]);
	    $_SESSION["info"] = $aviso;
	    header("location: index.php");
	} else {
	    //error
	    include('delete_view.html');
	}
	include('_preguntas.html');
	include('_footer.html'); 
?>