<?php 
	require_once('util.php');
	session_start();

	include('_header.html'); 

	if (isset($_POST["id"])) {
	    update($_POST["id"], $_POST["nombre"], $_POST["descripcion"], $_POST["anyo"], $_POST["categoria"], $_POST["desarrolladora"]);
	    $_SESSION["info"] = "Videojuego Actualizado";
	    header("location: index.php");
	} else  {
	    include('update_view.html'); 
	}
	include('_preguntas.html');
	include('_footer.html'); 

?>