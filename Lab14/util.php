<?php
	function conectDb(){
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "laboratorio";
		$con = mysqli_connect($servername,$username,$password,$dbname);
		//check connection
		if(!$con) {
			die("connection failed".mysqli_connect_error());
		}
		//echo "Connected successfully";
		return $con;
	}

	function closeDb($mysql){
		mysqli_close($mysql);
	}

	function getVideoGames(){
		$conn = conectDb();
		$sql = 'SELECT * FROM videojuegos';
		//$result = mysqli_query($conn,$sql);
		$result = $conn->query($sql);
		$table = '<table class="table table-hover table-bordered"><tr class="success"><th>IdVideoGame</th><th>nombre</th><th>descripcion</th><th>año</th><th>categoria</th><th>desarrolladora</th></tr>';
        $i=1;
        while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
	        $table = $table . "<tr>";
	        // use of numeric index
	        $table = $table . '</td>';
	        $table = $table . "<td>" .  $row[4];
	          // name of the column as associative index
	        $table = $table . '</td>';
	        $table = $table . "<td>" .  $row[0];
	        $table = $table . '</td>';
	        $table = $table . "<td>" .  $row[1];
	        $table = $table . '</td>';
	        $table = $table . "<td>" .  $row[2];
	        $table = $table . '</td>';
	        $table = $table . "<td>" .  $row[3];
	        $table = $table . '</td>';
	        $table = $table . "<td>" . $row[5];
	        $table = $table . '</td>';
	        $table = $table . '</tr>';
	        $i=$i+1;
        }
        $table = $table . '</table>';
           // it releases the associated results
         mysqli_free_result($result);
		closeDb($conn);
		return $table;
	}
?>