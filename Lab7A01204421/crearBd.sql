------------EJERCICIO 1---------

---Codigo para eliminar las tablas ya creadas
drop table Entregan 
drop table Materiales 
drop table Proyectos 
drop table Proveedores 

---Codigo para crear todas las tablas, incluyendo las llaves necesarias para cada una

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Materiales') 

DROP TABLE Materiales 

create table Materiales 
( 
  Clave numeric(5) not null, 
  Descripcion varchar(50), 
  Costo numeric(8,2) 
) 

---Hacer lo mismo para cada tabla. 
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proveedores') 

DROP TABLE Proveedores 

create table Proveedores 
( 
  RFC char(13) not null, 
  RazonSocial varchar(50) 
) 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proyectos') 

DROP TABLE Proyectos 

create table Proyectos 
( 
  Numero numeric(5) not null, 
  Denominacion varchar(50) 
) 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Entregan') 

DROP TABLE Entregan 

create table Entregan 
( 
  Clave numeric(5) not null, 
  RFC char(13) not null, 
  Numero numeric(5) not null, 
  Fecha DateTime not null, 
  Cantidad numeric(8,2) 
) 

---Codigo para cargar los datos a las tablas ya creadas

BULK INSERT a1204421.a1204421.[Materiales]
   FROM 'e:\wwwroot\a1204421\materiales.csv'
   WITH 
      (
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )

select * from materiales

BULK INSERT a1204421.a1204421.[Proyectos] 
  FROM 'e:\wwwroot\a1204421\proyectos.csv' 
  WITH 
  ( 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

  select * from Proyectos

BULK INSERT a1204421.a1204421.[Proveedores] 
  FROM 'e:\wwwroot\a1204421\proveedores.csv' 
  WITH 
  ( 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

SET DATEFORMAT dmy -- especificar formato de la fecha 
select * from Proveedores

BULK INSERT a1204421.a1204421.[Entregan] 
  FROM 'e:\wwwroot\a1204421\entregan.csv' 
  WITH 
  ( 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  )
  
  select * from Entregan

  select * from sysobjects where xtype='U'

  -------EJERCICIO 2 -------------------

  ---Agregar un nuevo registro a la tabla Materiales
  insert into Materiales values(1000, 'xxx', 1000) 
  select * from Materiales

  ---Eliminar el registro de la tabla Materiales
  Delete from Materiales where Clave = 1000 and Costo = 1000 
  select * from Materiales

  ---Definir una llave primaria para la tabla Materiales
  alter table Materiales add constraint llaveMateriales PRIMARY KEY (Clave) 

  ---Agregar nuevamente el registro a la tabla de materiales: 
  insert into Materiales values(1000, 'xxx', 1000) 

  ---Verificar si existe alguna restriccion en la tabla Materiales
  sp_helpconstraint Materiales 

  ---Definir una llave primaria para la tabla Proyectos
  alter table Proyectos add constraint llaveProyectos PRIMARY KEY (Numero) 
  sp_helpconstraint Proyectos

  ---Definir una llave primaria para la tabla Proveedores
  alter table Proveedores add constraint llaveProveedores PRIMARY KEY (RFC)
  sp_helpconstraint Proveedores
  
  ---Definir una llave primaria para la tabla Entregan
  alter table Entregan add constraint llaveEntregan PRIMARY KEY (Clave,RFC,Numero,Fecha)
  sp_helpconstraint Entregan
  
  
  -----------------EJERCICIO 3----------------------
  
  select * from Materiales;
  select * from Proyectos;
  select * from Proveedores;
  select * from Entregan; 

  ---Intentar insertar el siguiente registro en la tabla Entregan 
  insert into entregan values (0, 'xxx', 0, '1-jan-02', 0) ;
  select * from Entregan;  
  ---Anular el registro anterior
  Delete from Entregan where Clave = 0 
  select * from Entregan; 

  ---Agregar el siguiente constraint a la tabla Entregan
  alter table entregan add constraint cfentreganclave 
  foreign key (clave) references materiales(clave);
  sp_helpconstraint Entregan;
   select * from Entregan; 
   ---Intentar nuevamente insertar el siguiente registro en la tabla Entregan 
  insert into entregan values (0, 'xxx', 0, '1-jan-02', 0) ;

  ---Agregar el constraint de Entregan referenciando a Proyectos
  alter table Entregan add constraint cfentregannumero 
  foreign key (Numero) references Proyectos(Numero);
  sp_helpconstraint Entregan
  select * from Entregan; 

  ---Agregar el constraint de Entregan referenciando a Proveedores
  alter table Entregan add constraint cfentreganRFC 
  foreign key (RFC) references Proveedores(RFC);
  sp_helpconstraint Entregan
   select * from Entregan; 


----------------EJERCICIO 4-------------------

----Efectuar la siguiente sentencia insert: 
insert into entregan values (1000, 'AAAA800101', 5000, GETDATE(), 0); 
select * from Entregan
--- Eliminar el registro anterior
Delete from Entregan where Cantidad = 0 
select * from Entregan

---Para no permitir una cantidad igual a 0 utilizamos el siguiente constraint: 
alter table entregan add constraint cantidad check (cantidad > 0) ; 
---Probar si funciona la validacion de 0 
insert into entregan values (1000, 'AAAA800101', 5000, GETDATE(), 0);

---Verificar los constraints
sp_helpconstraint Materiales
sp_helpconstraint Proyectos
sp_helpconstraint Proveedores
sp_helpconstraint Entregan