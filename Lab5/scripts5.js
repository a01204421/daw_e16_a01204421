var subtotal=0;
var total=0;
var iva=0;
var elementosTotales=0;
var balonesTotales=0;
var guitarrasTotales=0;
var cubosTotales=0;

function usuarioNuevo(){
	var nombre = document.getElementById("nombreusu").value;
	var passw = document.getElementById("password").value;
	var passw2 = document.getElementById("password2").value;

	if(nombre.value == ""){
		alert("Ingrese su nombre");
		return false;
	}
	if(passw.value == ""){
		alert("Ingrese la contraseña");
		return false
	}
	if(passw2.value == ""){
		alert("Compruebe la contraseña");
		return false;
	}
	var nombreContenedor= document.getElementById("bienvenido");
	//nombreContenedor.innerHTML =  "Bienvenido "+nombre+" a mi pagina web";
	if(passw == passw2){
		alert("Bienvenido");
	}else{
		alert("Las contraseñas son distintas, prueba otra ves");
	}

}


function cobrar(){
	var precioCubo = document.getElementById("costo1");
	var precioBalon = document.getElementById("costo2");
	var precioGuitarra = document.getElementById("costo3");
	var cantidadTotalArticulos = document.getElementById("cantidadArticulos");
	var precioTotal = document.getElementById("total");
	var precioSubtotal = document.getElementById("subtotal");
	var precioIva = document.getElementById("iva");
	cantidadTotalArticulos.innerHTML = "Cantidad de articulos: "+elementosTotales+"<br>"+"Cantidad de guitarras: "+guitarrasTotales+"<br> Cantidad de cubos: "+cubosTotales+"<br> Cantidad de balones: "+balonesTotales;
	precioSubtotal.innerHTML = "Subtotal: $"+subtotal;
	precioIva.innerHTML = "IVA: $"+iva;
	precioTotal.innerHTML = "Total: $"+total;

}

function sumarCostoBalon(){
	subtotal = subtotal+650;
	iva = iva +(650*.15);
	total = total + (subtotal+iva);
	balonesTotales++;
	elementosTotales++;
	cobrar();
}

function sumarCostoCubo(){
	subtotal = subtotal+180;
	iva = iva +(180*.15);
	total = total + (subtotal+iva);
	cubosTotales++;
	elementosTotales++;
	cobrar();
}

function sumarCostoGuitarra(){
	subtotal = subtotal+3200;
	iva = iva +(3200*.15);
	total = total + (subtotal+iva);
	guitarrasTotales++;
	elementosTotales++;
	cobrar();
}

function quitarCostoBalon(){
	subtotal = subtotal-650;
	iva = iva -(650*.15);
	total = total - (subtotal-iva);
	balonesTotales--;
	elementosTotales--;
	cobrar();
}

function quitarCostoCubo(){
	subtotal = subtotal-180;
	iva = iva -(180*.15);
	total = total - (subtotal-iva);
	cubosTotales--;
	elementosTotales--;
	cobrar();
}

function quitarCostoGuitarra(){
	subtotal = subtotal-3200;
	iva = iva -(3200*.15);
	total = total - (subtotal-iva);
	guitarrasTotales--;
	elementosTotales--;
	cobrar();
}

function reloj(){
	var fecha = new Date();
	var hora = fecha.getHours();
	var min = fecha.getMinutes();
	var seg = fecha.getSeconds();
	if(min < 10 ){
		min = "0"+min;
	}
	if(seg < 10){
		seg = "0"+seg;
	}
	var recargar = setTimeout('reloj()',500);
	document.getElementById('bienvenido').innerHTML = "La hora es: "+hora + " : " + min + " : " +seg;
}

