BULK INSERT a1204421.a1204421.[Materiales]
   FROM 'e:\wwwroot\a1204421\materiales.csv'
   WITH 
      (
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )


BULK INSERT a1204421.a1204421.[Proveedores]
   FROM 'e:\wwwroot\a1204421\proveedores.csv'
   WITH 
      (
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )


BULK INSERT a1204421.a1204421.[Proyectos]
   FROM 'e:\wwwroot\a1204421\proyectos.csv'
   WITH 
      (
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )


BULK INSERT a1204421.a1204421.[Entregan]
   FROM 'e:\wwwroot\a1204421\entregan.csv'
   WITH 
      (
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )

SET DATEFORMAT dmy

SELECT * FROM Materiales
SELECT * FROM Proyectos
SELECT * FROM Proveedores
SELECT * FROM Entregan